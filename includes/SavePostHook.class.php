<?php
/**
 * Created by PhpStorm.
 * User: Rodrigo Murillo
 * Date: 3/21/14
 * Time: 8:36 AM
 */

class SavePostHook {

    protected $is_revision;
    protected $is_autosave;
    protected $status;
    protected $post_types;
    protected $callback;
    protected $priority;
    protected $num_args;

    function __construct( $callback, $post_types = array(), $priority = 10, $num_args = 1, $is_revision = FALSE, $is_autosave = FALSE, $status = array('publish') ) {

        try{

            if( !is_callable($callback )) {
                throw new Exception('Add function expects a valid callback. '. $callback . 'given.');
            }

            $this->callback = $callback;
            $this->is_revision = $is_revision;
            $this->is_autosave = $is_autosave;
            $this->status = $status;
            $this->post_types = $post_types;
            $this->priority = $priority;
            $this->num_args = $num_args;

            $this->addAction();

        }catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    function addAction() {
        add_action('save_post', array(&$this, 'theCallback'), $this->priority, $this->num_args);
    }

    function removeAction() {
        remove_action( 'save_post', array(&$this, 'theCallback'), $this->priority );
    }

    function theCallback ( $post_id, $post = null ) {

        $is_autosave     = wp_is_post_autosave( $post_id );
        $is_revision     = wp_is_post_revision( $post_id );
        $post_status     = get_post_status( $post_id );
        $post_type       = get_post_type( $post_id );

        $pass_autosave   = ($is_autosave === $this->is_autosave);
        $pass_revision   = ($is_revision === $this->is_revision);
        $pass_status     = in_array( $post_status, (array) $this->status );
        $pass_post_type  = in_array( $post_type, (array) $this->post_types );

        if( $pass_autosave && $pass_revision && $pass_status && $pass_post_type ) {
            call_user_func( $this->callback, $post_id, $post );
        }
    }
}