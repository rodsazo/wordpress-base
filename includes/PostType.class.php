<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 6/29/13
 * Time: 12:34 PM
 *
 * v 0.0.1
 * 23/12/13
 */

abstract class PostType extends ActionAdder{

    private $_post_type;
    private $_slug;
    /**
     * @var PostTypeLabels
     */
    protected $labels;

    protected $label = '[Custom Post Label]';
    protected $description = '';

    /**
     * @var bool
     * @default FALSE
     */
    protected $has_archive = FALSE;

    /**
     * @var bool
     * @default TRUE
     */
    protected $public = TRUE;

    /**
     * @var bool
     * @default TRUE
     */
    protected $show_ui = TRUE;

    /**
     * @var bool
     * @default TRUE
     */
    protected $show_in_menu = TRUE;

    /**
     * @var string
     * @default 'post'
     */
    protected $capability_type = 'post';

    /**
     * @var string
     * @default 'hierarchical'
     */
    protected $hierarchical = 'hierarchical';

    /**
     * @var bool
     * @default TRUE
     */
    protected $query_var = TRUE;

    /**
     * @var bool
     * @default FALSE
     */
    protected $exclude_from_search = FALSE;

    /**
     * @var array
     * @default array('title', 'editor');
     */
    protected $supports = array('title','editor');

    /**
     * @var array
     * @default array()
     */

    protected $taxonomies = array();


    protected $menu_icon;

    /*
     * FUNCTIONS
     * --------------------------------------------------------------------------------
     */

    protected abstract function setup();

    protected function setupLabels($label, $singular_name, $add_new, $add_new_item, $edit, $edit_item, $menu_name, $name, $not_found, $not_found_in_trash, $parent, $search_items, $view, $view_item){
        $this->label = $label;
        $this->labels->singular_name = $singular_name;
        $this->labels->add_new = $add_new;
        $this->labels->add_new_item = $add_new_item;
        $this->labels->edit = $edit;
        $this->labels->edit_item = $edit_item;
        $this->labels->menu_name = $menu_name;
        $this->labels->name = $name;
        $this->labels->not_found = $not_found;
        $this->labels->not_found_in_trash = $not_found_in_trash;
        $this->labels->parent = $parent;
        $this->labels->search_items = $search_items;
        $this->labels->view = $view;
        $this->labels->view_item = $view_item;
    }

    protected function registerActions(){}

    final function registerPostType() {


        $args = array(
            'label' => $this->label,
            'description' => $this->description,
            'has_archive' => $this->has_archive,
            'public' => $this->public,
            'show_ui' => $this->show_ui,
            'show_in_menu' => $this->show_in_menu,
            'capability_type' => $this->capability_type,
            'hierarchical' => $this->hierarchical,

            'rewrite' => array(
                'slug' => $this->_slug
            ),

            'taxonomies' => $this->taxonomies,

            'query_var' => $this->query_var,
            'exclude_from_search' => $this->exclude_from_search,
            'supports' => $this->supports,

            'labels' => (array)$this->labels
        );

        $args['menu_icon'] = $this->menu_icon;

        register_post_type($this->_post_type, $args);
    }


    final protected function registerPostTypeAction() {
        $this->add_action('init','registerPostType', 10);
    }

    function __construct($post_name, $slug){

        $this->_post_type = $post_name;
        $this->_slug = $slug;

        $this->labels = new PostTypeLabels();

        $this->setup();
        $this->registerPostTypeAction();
        $this->registerActions();
    }

    static function shouldDoSaveAction( $post_id, $post, $post_type = '') {

        $status = get_post_status($post_id);

        $is_revision = wp_is_post_revision( $post_id );

        $is_draft = ($status == 'draft');

        $is_auto_draft = ($status == 'auto-draft');

        $incorrect_post_type = FALSE;

        if(!empty($post_type)){
            $incorrect_post_type =  get_post_type($post_id) != $post_type;
        }

        $should_stop = $is_draft || $is_revision || $incorrect_post_type || $is_auto_draft;

        return !$should_stop;
    }

    function addColumnFilters($post_type, $function_columns, $function_display_column, $function_column_style = '') {
        add_filter('manage_edit-'.$post_type.'_columns', array(&$this, $function_columns));
        add_filter('manage_'.$post_type.'_posts_custom_column', array(&$this, $function_display_column));

        if(!empty($function_column_style)){
            add_action('admin_head', array(&$this, $function_column_style));
        }
    }

}