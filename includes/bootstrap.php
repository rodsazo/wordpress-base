<?php
/**
 * EVDG Bootstrap
 * v 0.1
 *
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 10/25/13
 * Time: 4:07 PM
 */


require_once 'evdg/eye-candy.php';
require_once 'evdg/common.php';
require_once 'evdg/generators.php';

/*
 * OVERLOAD LIBRARY
 * --------------------------------------------------------------------------------
 */

function __autoload( $class_name ){
    $library_name = TEMPLATEPATH . '/includes/'.$class_name.'.class.php';
    $package_name = TEMPLATEPATH . '/packages/'.$class_name.'/'.$class_name.'.class.php';

    if( file_exists( $library_name )){
        require_once $library_name;
    }else if( file_exists( $package_name )){
        require_once $package_name;
    }
}

/*
 * Cached constants
 */

define( 'TEMPLATE_URL', get_stylesheet_directory_uri() );
define( 'URL', get_bloginfo('url') );


