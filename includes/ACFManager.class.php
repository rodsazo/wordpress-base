<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 8/26/13
 * Time: 4:20 PM
 */

class ACFManager extends ActionAdder{
    protected $custom_fields;

    protected static $instance;


    protected function __construct() {
        $this->custom_fields = array();

        $this->add_action('acf/register_fields', 'instanceFields');
    }

    function doAddCustomField($class_name) {
        $this->custom_fields[] = $class_name;
    }

    function instanceFields() {
        foreach($this->custom_fields as $class_name){
            require_once get_template_directory() . '/custom-fields/'.$class_name.'.class.php';
            new $class_name();
        }
    }

    /*
     * Static functions
     */

    protected static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new self();
        }

        return self::$instance;
    }

    static function addCustomField( $class_field ){

        if(!strpos($class_field, 'CustomField')){
            $class_field .= 'CustomField';
        }

        self::getInstance()->doAddCustomField($class_field);
    }

}