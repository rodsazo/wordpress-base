<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 10/25/13
 * Time: 5:08 PM
 */


function evdg_paginator($pages, $current_page = 1, $found_posts = null, $posts_per_page = null, $range = 2)
{
    if($pages == 0){
        return FALSE;
    }
    $showitems = ($range * 2)+1;

    if(empty($found_posts) && !is_null($found_posts)){
        return FALSE;
    }

    $paginator = '';

    $label = 'Páginas';

    $label = apply_filters('evdg_paginator_label', $label);

    if(1 != $pages)
    {
        $paginator .= '<div class="paginator">';

        if(!empty($label)){
            $paginator .= '<span class="paginator__label">'.$label.'</span>';
        }

        if($current_page > 2 && $current_page > $range+1 && $showitems < $pages) $paginator .= "<a class='paginator__link' href='".link_pagina(1)."'>&laquo;</a>";
        if($current_page > 1 && $showitems < $pages) $paginator .= "<a class='paginator__link' href='".link_pagina($current_page - 1)."'>&lsaquo;</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $current_page+$range+1 || $i <= $current_page-$range-1) || $pages <= $showitems ))
            {
                $paginator .= ($current_page == $i)? "<span class='paginator__current'>".$i."</span>":"<a class='paginator__link' href='".link_pagina($i)."'>".$i."</a>";
            }
        }

        if ($current_page < $pages && $showitems < $pages) $paginator .= "<a class='paginator__link' href='".link_pagina($current_page + 1)."'>&rsaquo;</a>";
        if ($current_page < $pages-1 &&  $current_page+$range-1 < $pages && $showitems < $pages) $paginator .= "<a class='paginator__link' href='".link_pagina($pages)."'>&raquo;</a>";

        // Post count

        if(!empty($found_posts)){

            $page_start = ($current_page - 1) * $posts_per_page + 1;
            $page_end = $current_page * $posts_per_page;

            $page_end = $page_end > $found_posts ? $found_posts : $page_end;

            $paginator .= sprintf('<span class="paginator__post-count">%1$s &ndash; %2$s %3$s %4$s</span>', $page_start, $page_end, __('de'), $found_posts);

        }

        $paginator .= "</div>\n";
    }

    return $paginator;
}

function link_pagina($numero = 1){
    $page_param = __('pagina');

    $page_param = apply_filters('evdg_paginator_param', $page_param);

    $url = ampliarUrl(array($page_param => $numero));
    return $url;
}

function ampliarUrl($args,$ban = ''){
    $query = array();
    $ban = (array) $ban;

    $ban = array_merge(array(__('pagina')),$ban);

    $valores = explode('&',$_SERVER['QUERY_STRING']);

    foreach($valores as $valor){
        if(empty($valor)){
            continue;
        }
        list($var,$val) = explode('=',$valor);
        if(!in_array($var,$ban)){
            $query[$var] = $val;
        }
    }

    $query = array_merge($query,$args);

    $query_sub = array();
    foreach($query as $var => $val){
        if(!$var || !$val){
            continue;
        }
        $query_sub[] = $var . '=' . $val;
    }

    return '?'.implode('&',$query_sub);
}