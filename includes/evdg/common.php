<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 10/25/13
 * Time: 5:06 PM
 */

/*
 * HELPER FUNCTIONS
 * --------------------------------------------------------------------------------
 */

/*
 * Sending request via POST
 */

function doingPostRequest() {
    return strtolower($_SERVER['REQUEST_METHOD']) == 'post';
}


/*
 * Helpers for $_GET and $_POST
 */

function _g($key, $default = FALSE){
    $value = isset($_GET[$key]) ? $_GET[$key] : $default;
    return strip_tags($value);
}

function _p($key, $default = FALSE){
    return isset($_POST[$key]) ? $_POST[$key] : $default;
}

/*
 * TEMPLATING FUNCTIONS
 * --------------------------------------------------------------------------------
 */

/**
 * Trims a given string to the number of characters allowed. Doesn't break words.
 * Adds a hellipsis (...) at the end, via &hellip;
 * @param $max_characters
 * @param string $string
 * @return string
 */

function evdg_contentPreview($max_characters = -1, $string = '' ) {

    if($string == ''){
        $string = get_the_content();
    }

    $string = strip_tags($string);

    if($max_characters !== -1){

        $partes = explode(' ', $string);

        $resumen = array();
        $cuenta = 0;

        foreach($partes as $pieza){
            $resumen[] = $pieza;
            $cuenta += (strlen($pieza) + 1); // 1 más por el espacio
            if($cuenta >= $max_characters){
                break;
            }
        }

        if(count($resumen) < count($partes)){
            $string = implode(" ",$resumen).'&hellip;';
        }

    }

    $string = preg_replace('/\[.+\]/','', $string);
    $string = str_replace(']]>', ']]&gt;', $string);

    return $string;
}

function evdg_dateRange($start, $end){
    list($s_year, $s_month, $s_day) = explode('-', $start);
    list($e_year, $e_month, $e_day) = explode('-', $end);

    $same_year = $s_year == $e_year;
    $same_month = $s_month == $e_month && $same_year;

    $s_month = mysql2date('F', $start);
    $e_month = mysql2date('F', $end);

    if($same_month){
        return sprintf('Del %1$s al %2$s de %3$s', $s_day, $e_day, $s_month);
    }else{
        return sprintf('Del %1$s de %2$s al %3$s de %4$s', $s_day, $s_month, $e_day, $e_month);
    }
}

function evdg_modifyDate ( $original, $modification, $format){
    $original_time = strtotime( $original );
    $modded_time = strtotime( $modification, $original_time );

    return date( $format, $modded_time );
}

function evdg_whiteTextFor( $hex_color , $contrast_factor = 130){

    $r = hexdec(substr($hex_color,1,2));
    $g = hexdec(substr($hex_color,3,2));
    $b = hexdec(substr($hex_color,5,2));

    $contrast = sqrt(
        $r * $r * .241 +
        $g * $g * .691 +
        $b * $b * .068
    );

    $white = ($contrast <= $contrast_factor);

    return $white;
}


function evdg_hex2rgb($hex) {
    $hex = str_replace("#", "", $hex);
    $color = array();

    if(strlen($hex) == 3) {
        $color['r'] = hexdec(substr($hex, 0, 1) . $r);
        $color['g'] = hexdec(substr($hex, 1, 1) . $g);
        $color['b'] = hexdec(substr($hex, 2, 1) . $b);
    }
    else if(strlen($hex) == 6) {
        $color['r'] = hexdec(substr($hex, 0, 2));
        $color['g'] = hexdec(substr($hex, 2, 2));
        $color['b'] = hexdec(substr($hex, 4, 2));
    }

    return $color;
}

function evdg_rgb2hex($r, $g, $b) {
    //String padding bug found and the solution put forth by Pete Williams (http://snipplr.com/users/PeteW)
    $hex = "#";
    $hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
    $hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
    $hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);

    return $hex;
}

/*
 * WORDPRESS IMPROVEMENTS
 * --------------------------------------------------------------------------------
 */

function get_all_posts($args = array()) {
    $args = array_merge(array(
        'numberposts' => -1,
        'posts_per_page' => -1
    ), $args);

    return get_posts( $args );
}