<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 10/25/13
 * Time: 5:04 PM
 */

/*
 * EYE CANDY
 * --------------------------------------------------------------------------------
 */

/*
 * Admin Styles
 */

function evdg_adminStyles() {
    add_editor_style( 'admin-styles.css' );
}
add_action( 'init', 'evdg_adminStyles' );


function evdg_login_logo() {
    /*
     * W: 274
     * H: 63
     */

    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/wordpress.gif) !important; }
    </style>';
}

add_action('login_head', 'evdg_login_logo');



