<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 6/12/13
 * Time: 3:04 PM
 *
 * v. 0.0.5
 */

class Translation extends ActionAdder{

    const TRANSLATION_ID = 'translation_id-';
    const ORIGINAL_ID = 'original_id';
    const SESSION_LANGUAGE = 'evdg-language';

    const SHOW_IN_MENU = FALSE;

    protected $original_post_type;
    protected $translation_post_type;
    protected $menu_name;
    protected $slug;
    protected $language;
    protected $has_archive;
    protected $public_name;

    protected static $index = array();
    protected static $index_from_original = array();
    protected static $language_index = array();

    protected static $title_field = 'title';
    protected static $lang_folder;
    protected static $lang_context;
    protected static $default_language;
    protected static $native_language;

    protected static $query_parameter;

    protected static $get_the_ID;
    /*
     * Setup
     */

    /**
     * @param string $lang_folder
     * @param string $lang_context
     * @param string $default_language
     * @param string $lang_query_param
     */

    static function setup( $lang_folder, $lang_context, $default_language, $lang_query_param){
        self::setTranslationsFolder( $lang_folder );
        self::setContextString( $lang_context );
        self::setDefaultLanguage( $default_language );
        self::setLanguageQueryParam( $lang_query_param );
    }

    static function translatePagesTo( $language ){
        new Translation('page', $language, 'page-'.$language, 'Translated Page', $language, FALSE);
    }

    /**
     * @param string $folder path from the template dir
     */

    static function setTranslationsFolder( $folder ){
        self::$lang_folder = $folder;
    }

    static function setContextString( $context ){
        self::$lang_context = $context;
    }

    static function setDefaultLanguage ( $language ){
        self::$default_language = $language;
    }

    static function setLanguageQueryParam ( $query_param ){
        self::$query_parameter = $query_param;
    }

    static function setTitleField( $title_field ){
        self::$title_field = $title_field;
    }

    /*
     * Initialization
     */

    static function init() {
        add_action('wp',array('Translation', 'doInit'));
    }

    static function doInit(){
        $selected_language = _g(self::$query_parameter);

        if($selected_language){

            self::setLanguage($selected_language);
            header('location: '.get_bloginfo('url'));
            exit();

        }else if(self::isTranslation()){

            $trans_object = self::getTranslationObject();
            self::setLanguage( $trans_object->getLanguage() );

        }else if(is_home() || is_404()){
            self::setLanguage( self::getCurrentLanguage() );
        }else{
            self::setLanguage( self::$default_language );
        }
    }

    protected static function getTranslationObject($post_type = '') {
        if(empty($post_type)){
            $post_type = get_query_var('post_type');
        }

        return self::getFromIndex($post_type);
    }

    /*
     * SESSION FUNCTIONS
     * --------------------------------------------------------------------------------
     */

    static function getCurrentLanguage() {

        @session_start();

        if(isset($_SESSION[ self::SESSION_LANGUAGE ])){
            return $_SESSION[ self::SESSION_LANGUAGE ];
        }

        return self::$default_language;
    }

    protected static function setLanguage( $lang ) {

        @session_start();

        $_SESSION[ self::SESSION_LANGUAGE ] = $lang;

        $success = self::setDomain($lang);
        return $success;
    }

    protected static function setDomain($lang) {
        $mo_file = get_template_directory() . '/' . self::$lang_folder . '/' . strtolower($lang) . '.mo';
        $success = load_textdomain(LANG_CONTEXT, $mo_file);
        return $success;
    }

    /*
     * INDEX FUNCTIONS
     * --------------------------------------------------------------------------------
     */

    protected static function addToIndex($post_type, $translation, $original){
        self::$index[$post_type] = $translation;
        self::$index_from_original[$original] = $translation;
    }

    /**
     * @param string
     * @return Translation|Boolean
     */

    protected static function getFromIndex( $post_type = ''){
        if(empty($post_type)){
            $post_type = get_query_var('post_type');
        }

        if( isset( self::$index[$post_type] )){
            return self::$index[$post_type];
        }
        return FALSE;
    }

    protected static function addToLanguageIndex( $post_type, $language, $translated_post_type ){
        if(!isset(self::$language_index[$post_type])){
            self::$language_index[$post_type] = array(
                self::$default_language => $post_type
            );
        }
        self::$language_index[$post_type][$language] = $translated_post_type;

        return TRUE;
    }

    protected static function getFromLanguageIndex( $original_post_type, $language ){
        if(isset(self::$language_index[$original_post_type][$language])){
            return self::$language_index[$original_post_type][$language];
        }
        return FALSE;
    }

    /**
     * @param string
     * @return Translation|Boolean
     */

    protected static function getFromOriginal( $post_type ){
        if( isset( self::$index_from_original[$post_type] )){
            return self::$index_from_original[$post_type];
        }
        return FALSE;
    }

    /*
     * HELPER FUNCIONS
     * --------------------------------------------------------------------------------
     */

    static function getMenuName($post_type = '') {

        $object = self::getFromIndex($post_type);

        if($object){
            return $object->menu_name;
        }

        return FALSE;
    }

    protected static function getOriginalPostType($post_type = ''){

        $object = self::getFromIndex($post_type);

        if($object){
            return $object->doGetOriginalPostType();
        }

        return FALSE;

    }

    static function setupTranslatedPost($id = null) {
        global $post;
        static $translated_post;

        $id = is_null($id) ? get_the_ID() : $id;

        if(!isset($translated_post)){
            $post_id = Translation::getOriginalPostID($id);
            $translated_post = get_post($post_id);
        }

        $post = $translated_post;

        self::$get_the_ID = $translated_post->ID;
        setup_postdata($translated_post);

    }


    /*
     * THEME FUNCTIONS
     * --------------------------------------------------------------------------------
     */

    static function resetPostData() {
        global $post;

        wp_reset_postdata();
        self::setupTranslatedPost();

    }


    static function get_title($post_id = '') {

        if(empty($post_id)){
            $post_id = get_the_ID();
        }

        if( self::getCurrentLanguage() != self::$default_language){

            $post_type = get_post_type($post_id);
            $translation_object = Translation::getFromOriginal( $post_type );

            return get_post_meta($post_id, $translation_object->getTranslationTitleKey(), true);
        }

        return get_the_title($post_id);

    }

    static function getOriginalPostId($id = null) {
        $id = is_null( $id ) ? get_the_ID() : $id;
        $post_id = get_post_meta($id, self::ORIGINAL_ID, true);
        return $post_id;
    }

    static function getTranslatedPostId($id = null, $lang = null) {

        if( empty($lang) ){
            $lang = self::getCurrentLanguage();
        }

        if(empty($id)){
            $id = get_the_ID();
        }

        if( $lang == self::$default_language ){
            return $id;
        }

        return get_post_meta($id, self::TRANSLATION_ID.$lang, true);
    }

    static function fieldName ( $field_name ){

        // Returns "ES" or "EN"
        $lang_sufix = Translation::getCurrentLanguage();

        $field_name = $field_name . "-" . $lang_sufix;

        return $field_name;
    }

    static function get_field ( $field_name, $id = null ){
        $field_value = get_field(self::fieldName($field_name), $id);

        if(!$field_value){
            $field_value = get_field( $field_name . '-' . self::$default_language, $id);
        }

        return $field_value;
    }

    static function the_field ($field_name, $id = null ){
        the_field ( self::fieldName( $field_name ), $id ) ;
    }

    static function get_sub_field ( $field_name){
        $value = get_sub_field(self::fieldName($field_name));

        if(!$value){
            $value = get_sub_field( $field_name . '-' . self::$default_language);
        }

        return $value;
    }

    static function the_sub_field ($field_name){
        echo self::get_sub_field( $field_name ) ;
    }

    /**
     * Siempre se trabaja sobre un post no traducido (en loop),
     * por lo tanto, el valor por defecto es el permalink del post
     * original.
     *
     * @param string $post_id
     * @param string $language
     * @return string
     */

    static function get_permalink($post_id = '') {

        $language = self::getCurrentLanguage();

        if(empty($post_id)){
            $post_id = get_the_ID();
        }

        $the_post = get_post($post_id);

        $post_type = $the_post->post_type;

        $translation_object = Translation::getFromOriginal( $post_type );

        if( $language != self::$default_language){

            $translation_id = get_post_meta( $post_id, $translation_object->getTranslationIDKey(), true);
            return get_permalink($translation_id);
        }
        return get_permalink($post_id);
    }

    static function the_title($main_title = FALSE) {
        $id = '';
        if($main_title && isset(self::$get_the_ID)){
            $id = self::$get_the_ID;
        }
        echo self::get_title($id);
    }

    static function the_permalink(){
        echo self::get_permalink();
    }

    static function isTranslation($current_post_type = ''){

        static $is_translation;

        if(isset($is_translation)){
            return $is_translation;
        }

        if(empty($current_post_type)){
            $current_post_type = get_query_var('post_type');
        }

        $is_translation = FALSE;

        foreach( self::$index as $post_type => $languages){
            if($current_post_type == $post_type){
                $is_translation = TRUE;
            }
        }

        return $is_translation;
    }

    static function getLanguageLink( $language ){

        static $original_post_type;
        static $original_post_id;

        $lang_link = '#';

        if(is_home() || is_search() || is_404()){

            $lang_link = sprintf('%1$s?%2$s=%3$s', get_bloginfo('url'), self::$query_parameter, $language);

        }else if(is_archive()){

            if(!isset($original_post_type)){
                $original_post_type = get_query_var('post_type');

                if(self::isTranslation($original_post_type)){
                    $original_post_type = self::getOriginalPostType($original_post_type);
                }
            }

            $link_post_type = self::getFromLanguageIndex( $original_post_type, $language );
            $link_post_type_obj = get_post_type_object( $link_post_type );

            $lang_link = sprintf('%1$s/%2$s/', get_bloginfo('url'), $link_post_type_obj->rewrite['slug'] );

        }else if(is_singular()){

            if(!isset($original_post_id)){
                $original_post_id = get_the_ID();
                if(self::isTranslation()){
                    $original_post_id = self::getOriginalPostId( $original_post_id );
                }
            }

            $translation_id = $original_post_id;

            if($language != self::$default_language){
                $translation_id = self::getTranslatedPostId($original_post_id, $language);
            }

            $lang_link = get_permalink( $translation_id );

        }

        return $lang_link;

    }

    /*
     * CONSTRUCTOR
     * --------------------------------------------------------------------------------
     */

    function __construct($post_type, $language, $translation_post_type, $menu_name, $slug, $has_archive = TRUE, $has_slug = TRUE) {
        $this->language = $language;
        $this->original_post_type = $post_type;
        $this->translation_post_type = $translation_post_type;
        $this->menu_name = $menu_name;
        $this->slug = $slug;
        $this->has_archive = $has_archive;

        $this->registerTranslation();
        $this->registerTemplateFileFilter();
        $this->registerActions();

        self::addToIndex($translation_post_type, $this, $post_type);
        self::addToLanguageIndex( $post_type, $language, $translation_post_type);
    }

    protected function registerTemplateFileFilter(){
        $this->add_filter('single_template', 'changeSingleTemplate', 10, 1);
        $this->add_filter('archive_template', 'changeArchiveTemplate', 10, 1);
    }

    function changeSingleTemplate( $single_template ){
        global $post;

        if( $post->post_type == $this->translation_post_type ){

            if($this->original_post_type == 'page'){
                return $this->changePageTemplate();
            }

            $single_template = 'single-' . $this->original_post_type.'.php';

            $single_template = locate_template($single_template);

            self::setupTranslatedPost();

        }

        return $single_template;
    }

    protected function changePageTemplate(){
        global $post;

        $original_id = self::getOriginalPostId($post->ID);
        $original_post = get_post($original_id);

        $pagename = $original_post->post_name;
        $id = $original_post->ID;

        $templates = array();

        $templates[] = "page-$pagename.php";
        $templates[] = "page-$id.php";
        $templates[] = 'page.php';

        self::setupTranslatedPost();

        return locate_template($templates);
    }

    function changeArchiveTemplate( $archive_template ){
        global $post;

        if( $post->post_type == $this->translation_post_type ){
            $archive_template = dirname( $archive_template ) . '/archive-' . $this->original_post_type.'.php';
        }

        return $archive_template;
    }

    function registerTranslation () {

        register_post_type($this->translation_post_type, array(
            'label' => $this->menu_name,
            'description' => '',
            'has_archive' => $this->has_archive,
            'public' => true,
            'show_ui' => self::SHOW_IN_MENU,
            'show_in_menu' => self::SHOW_IN_MENU,
            'capability_type' => 'post',
            'hierarchical' => false,

            'rewrite' => array(
                'slug' => $this->slug
            ),

            'query_var' => true,
            'exclude_from_search' => false,
            'supports' => array('title','editor'),

            'menu_position' => 20,

            'labels' => array (
                'name' => $this->menu_name,
                'singular_name' => $this->menu_name,
                'menu_name' => $this->menu_name,
                'add_new' => 'Add $translation_name',
                'add_new_item' => 'Add new $translation_name',
                'edit' => 'Edit',
                'edit_item' => 'Edit $translation_name',
                'new_item' => 'New $translation_name',
                'view' => 'View',
                'view_item' => 'View $translation_name',
                'search_items' => 'Search $translation_label',
                'not_found' => 'No $translation_label were found',
                'not_found_in_trash' => 'No $translation_label were found in trash',
                'parent' => 'Parent $translation_name'
            )
        ) );

    }

    function doGetOriginalPostType(){
        return $this->original_post_type;
    }

    function getLanguage(){
        return $this->language;
    }

    /*
     * META KEYS
     * --------------------------------------------------------------------------------
     */

    /**
     * Returns translation_id meta for THIS LANGUAGE
     * @return string
     */

    function getTranslationIDKey() {
        return self::TRANSLATION_ID . $this->language;
    }

    function getTranslationTitleKey() {
        return self::$title_field . '-' . $this->language;
    }

    /*
     * HOOKING ACTIONS
     * --------------------------------------------------------------------------------
     */

    function registerActions() {
        $this->registerSaveActions();

        add_action('before_delete_post', array(&$this, 'deleteTranslation') , 99 , 1);
    }

    function registerSaveActions() {
        add_action('save_post', array(&$this, 'saveTranslation'), 99, 2);
    }

    function removeSaveActions() {
        remove_action('save_post', array(&$this, 'saveTranslation') , 99, 2);
    }


    /*
     * SAVE TRANSLATION
     * --------------------------------------------------------------------------------
     */

    function saveTranslation($post_id, $post) {

        $should_i_save = PostType::shouldDoSaveAction($post_id, $post, $this->original_post_type);
        $the_post = get_post($post_id);

        if( $should_i_save ){

            $translation_key = $this->getTranslationIDKey();
            $title_key = $this->getTranslationTitleKey();

            $translation_id = get_post_meta($post_id, $translation_key, true);
            $translation_title = get_post_meta($post_id, $title_key, true);

            if(empty($translation_title)){
                $translation_title = $the_post->post_title;
                update_post_meta($post_id, $title_key, $translation_title);
            }

            $translation_name = sanitize_title($translation_title);

            $this->removeSaveActions();

            $translation_id = wp_insert_post(array(
                'ID' => $translation_id,
                'post_type' => $this->translation_post_type,
                'post_status' => $post->post_status,
                'post_title' => $translation_title,
                'post_name' => $translation_name,
                'post_parent' => $the_post->post_parent
            ));

            /*
             * Save post meta por post and translation
             */

            update_post_meta($post_id, $translation_key, $translation_id);
            update_post_meta($translation_id, self::ORIGINAL_ID, $post_id);

            $this->registerSaveActions();

        }
    }

    /*
     * DELETE TRANSLATIONS
     * --------------------------------------------------------------------------------
     */

    /**
     * Called on 'before_delete_post'
     * @param $post_id
     */

    function deleteTranslation( $post_id ) {

        if(get_post_type($post_id) == $this->original_post_type){

            $translation_key = $this->getTranslationIDKey();
            $translation_id = get_post_meta( $post_id, $translation_key, true);

            wp_delete_post( $translation_id );

        }
    }


}