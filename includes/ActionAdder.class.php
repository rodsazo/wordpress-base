<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 8/3/13
 * Time: 3:59 PM
 */


class ActionAdder{
    final protected function add_action($hook_name, $function_name, $priority = null, $accepted_args = null){
        add_action($hook_name, array(&$this, $function_name), $priority, $accepted_args);
    }

    final protected function add_filter( $hook_name, $function_name, $priority = null, $accepted_args = null){
        add_filter($hook_name, array(&$this, $function_name), $priority, $accepted_args);
    }


    final protected function remove_action($hook_name, $function_name, $priority = null){
        remove_action($hook_name, array(&$this, $function_name), $priority);
    }

    final protected function remove_filter( $hook_name, $function_name, $priority = null){
        remove_filter($hook_name, array(&$this, $function_name), $priority);
    }
}