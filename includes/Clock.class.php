<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 12/20/13
 * Time: 6:10 PM
 *
 * v.0.0.1
 */

class Clock {

    protected $column_number;
    protected $current_col;

    function __construct($column_number){

        $this->column_number = $column_number;

        $this->current_col = 0;
    }

    function reset(){
        $this->current_col = 0;
    }

    function printEveryFirst( $string ){
        if( $this->isFirst() ){
            echo $string;
        }
    }

    function printEveryLast( $string ){
        if( $this->isLast() ){
            echo $string;
        }
    }

    function printEveryN( $n, $string ){
        if( $this->isN($n) ){
            echo $string;
        }
    }

    function isFirst(){
        return $this->isN(0);
    }

    function isLast(){
        return $this->isN( $this->column_number - 1 );
    }

    function isN( $n ){

        $ret =  $this->current_col == $n;

        $this->tick();
        return $ret;

    }

    function tick(){
        $this->current_col = ($this->current_col + 1) % $this->column_number;
    }

    function econo_columnClass(){
        global $_ecoColNumber, $_ecoFirstColClass, $_ecoCurrentCol;

        if($_ecoCurrentCol == 0){
            echo $_ecoFirstColClass;
        }

        $_ecoCurrentCol = ( $_ecoCurrentCol + 1 ) % $_ecoColNumber;
    }
} 