<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 8/17/13
 * Time: 5:27 PM
 */

class PostTypeLabels {
    public $name = '[Custom Post Labels Type Name]';
    public $singular_name = '[Custom Post Labels Singular Name]';
    public $menu_name = '[Custom Post Labels Menu Name]';
    public $add_new = '[Custom Post Labels Add New]';
    public $add_new_item = '[Custom Post Labels Add New Item]';
    public $edit = '[Custom Post Labels Edit]';
    public $edit_item = '[Custom Post Labels Edit Item]';
    public $view = '[Custom Post Labels View]';
    public $view_item = '[Custom Post Labels View Item]';
    public $search_items = '[Custom Post Labels Search Items]';
    public $not_found = '[Custom Post Labels Not Found]';
    public $not_found_in_trash = '[Custom Post Labels Not Found In Trash]';
    public $parent = '[Custom Post Labels Parent]';
}