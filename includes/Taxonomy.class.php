<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 6/29/13
 * Time: 12:34 PM
 */

abstract class Taxonomy extends ActionAdder{

    protected $tax_name;
    protected $object_type;

    protected $labels;

    protected $public = TRUE;

    protected $show_ui = null;

    protected $show_in_nav_menus = null;

    protected $show_tagcloud = null;

    protected $show_admin_column = FALSE;

    protected $hierarchical = FALSE;

    protected $update_count_callback = '';

    protected $query_var = null;

    protected $rewrite = TRUE;

    protected $rewrite_slug = null;
    protected $rewrite_with_front = null;
    protected $rewrite_hierarchical = null;
    protected $rewrite_ep_mask = null;

    protected $capabilities = array();
    protected $sort = null;

    abstract function setup();

    final function registertaxonomy(){

        $rewrite = TRUE;

        $rewrite_vars = explode(",",'slug,with_front,hierarchical,ep_mask');

        foreach($rewrite_vars as $rwvar){
            $var_name = 'rewrite_'.$rwvar;
            if(!is_null($this->$var_name)){
                if(!is_array($rewrite)){
                    $rewrite = array();
                }
                $rewrite[$rwvar] = $this->$var_name;
            }
        }

        $args = array(
            'labels' => $this->labels,
            'public' => $this->public,
            'show_ui' => $this->show_ui,
            'show_in_nav_menus' => $this->show_in_nav_menus,
            'show_in_tagcloud' => $this->show_tagcloud,
            'show_admin_column' => $this->show_admin_column,
            'hierarchical' => $this->hierarchical,
            'update_count_callback' => $this->update_count_callback,
            'query_var' => $this->query_var,
            'rewrite' => $rewrite,
            'capabilities' => $this->capabilities,
            'sort' => $this->sort
        );

        register_taxonomy($this->tax_name, $this->object_type, $args);

        foreach((array) $this->object_type as $post_type) {
            register_taxonomy_for_object_type( $this->tax_name, $post_type );
        }
    }

    final protected  function registerTaxonomyAction(){
        $this->add_action('init', 'registerTaxonomy', 11);
    }

    protected function registerActions(){}

    function __construct($tax_name, $slug, $object_type){
        $this->labels = array();
        $this->object_type = $object_type;
        $this->rewrite_slug = $slug;
        $this->tax_name = $tax_name;
        $this->setup();
        $this->registerTaxonomyAction();
        $this->registerActions();
    }

    function setupLabels($name, $singular_name, $menu_name, $all_items, $edit_item, $view_item, $update_item, $add_new_item, $new_item_name, $parent_item, $parent_item_colon, $search_items, $popular_items, $separate_items_with_commas, $add_or_remove_items, $choose_from_most_used, $not_found){
        $label_list = explode(",", 'name,singular_name,menu_name,all_items,edit_item,view_item,update_item,add_new_item,new_item_name,parent_item,parent_item_colon,search_items,popular_items,separate_items_with_commas,add_or_remove_items,choose_from_most_used,not_found');

        foreach($label_list as $var_name){
            $this->labels[$var_name] = $$var_name;
        }

    }

}