<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 3/14/14
 * Time: 3:42 PM
 */

class MailInliner {

    protected $contents;
    protected $styles_array;

    function __construct( $filename_or_contents ) {

        $contents = $filename_or_contents;

        if( file_exists( $filename_or_contents ) ) {
            $contents = file_get_contents( $filename_or_contents );
        }

        // Capture all styles

        preg_match_all('/\.([^{^}]+){([^}]+)}/', $contents, $style_matches );

        // Remove style tag

        $contents = preg_replace('/<style[^>]*>[^<]*<\/style>/', '', $contents);

        // Replace classes with styles

        for ( $i = 0, $length = count( $style_matches[1]); $i < $length; $i ++ ) {
            $inline_styles = str_replace(array( "  ", "\n", "\r" ), '', $style_matches[2][$i]);
            $contents = str_replace('class="'.$style_matches[1][$i].'"', 'style="' . $inline_styles . '"', $contents );
        }

        $this->contents = $contents;

    }

    /**
     * @return mixed
     */

    public function getContents()
    {
        return $this->contents;
    }

}