<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 10/25/13
 * Time: 11:21 PM
 *
 * v.0.0.1
 * 20/1/14
 *
 * Añadí Output fileniame
 */


class JSConcat{

    protected static $js_files = array();
    protected static $start;
    protected static $end;
    protected static $outputfile = 'js-concat';

    protected static function addFileOfType ($path, $type = 'file'){
        self::$js_files[] = array(
            'path' => $path,
            'type' => $type
        );
    }

    static function addFile($path_from_template_url){
        self::addFileOfType($path_from_template_url);
    }

    static function addAnonymous($path_from_template_url){
        self::addFileOfType($path_from_template_url, 'anonymous');
    }

    static function setOutputFilename( $name ){
        self::$outputfile = $name;
    }

    static function printScripts($debug_time){

        self::$start = microtime(TRUE);

        $last_modified = self::getLastModified();
        $last_cached = self::getLastCached();

        $need_new_cache = strcmp($last_modified, $last_cached);

        if ($need_new_cache > 0) {
            self::rewriteCache();
            $last_cached = date('Y-m-d H:i:s');
        }

        printf('<script type="text/javascript" src="%1$s?ld=%2$s"></script>', TEMPLATE_URL.'/' . self::$outputfile . '.js', urlencode(str_replace(':', '', $last_cached)));
        self::$end = microtime(TRUE);

        if($debug_time){
            printf('<!-- Script generado en %1$d segundos -->', (self::$end - self::$start));
        }
    }

    protected static function getLastModified(){
        $last_mod = 0;

        foreach (self::$js_files as $file){

            $file_path = get_stylesheet_directory() . '/' . $file['path'];

            if(file_exists($file_path)){

                $file_mod = filemtime( $file_path );
                $last_mod = $file_mod > $last_mod ? $file_mod : $last_mod;

            }
        }

        return date('Y-m-d H:i:s', $last_mod);
    }

    protected static function getLastCached(){
        $cached_file = get_template_directory() . '/' . self::$outputfile . '.js';

        if (file_exists( $cached_file )) {
            $last_cache = date('Y-m-d H:i:s', filemtime( $cached_file ));
        }else{
            $last_cache = '0000-00-00 00:00:00';
        }

        return $last_cache;
    }

    protected static function rewriteCache() {
        $cached_file = get_template_directory() . '/' . self::$outputfile . '.js';

        if( file_exists($cached_file) ){
            unlink($cached_file);
        }

        $cache_handle = fopen( $cached_file, 'w' );
        $cache_str = array();

        foreach( self::$js_files as $file ){
            $file_path = get_stylesheet_directory() . '/' . $file['path'];

            if(file_exists($file_path)){
                $is_anonymous = $file['type'] == 'anonymous';

                if($is_anonymous){
                    $cache_str[] = '(function(){';
                }

                $cache_str[] = file_get_contents( $file_path );

                if($is_anonymous){
                    $cache_str[] = '}());';
                }
            }
        }

        $cache_str = implode(';', $cache_str);

        fwrite( $cache_handle, $cache_str );


    }

}