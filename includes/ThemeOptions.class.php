<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 11/29/13
 * Time: 11:11 AM
 */

class ThemeOptions {

    final static function get_field( $field_name ){
        return get_field( $field_name, 'options');
    }

    final static function the_field( $field_name ){
        the_field( $field_name, 'options');
    }

    final static function has_sub_field( $field_name ){
        return has_sub_field( $field_name, 'options');
    }

} 