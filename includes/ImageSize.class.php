<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rodrigomurillo
 * Date: 7/7/13
 * Time: 2:25 PM
 */

class ImageSize {

    public $width;
    public $height;
    public $name;
    public $crop;

    private static $index = array();

    function __construct ($name, $width, $height, $crop = 0){
        $this->name = $name;
        $this->width = $width;
        $this->height = $height;
        $this->crop = $crop;

        add_image_size($name, $width, $height, $crop);
        self::addToIndex($name, $this);
    }

    private static function addToIndex ($name, $size_object){
        self::$index [ $name ] = $size_object;
    }

    private static function getSize ( $name ) {
        return self::$index [ $name ];
    }


}