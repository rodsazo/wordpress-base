<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 12/20/13
 * Time: 6:06 PM
 */

/*
 * Bootstrap
 */

require_once 'includes/bootstrap.php';

/*
 * HIDE UNUSED ITEMS
 */

function remove_menu_pages () {
    remove_menu_page('link-manager.php');
    remove_menu_page('edit.php');
}

add_action( 'admin_menu', 'remove_menu_pages' );

/*
 * OPTIONS PAGES
 */

if( function_exists('acf_add_options_sub_page') )
{

    acf_set_options_page_title( 'Opciones D.E.');

    acf_add_options_sub_page(array(
        'title' => 'Open Graph y SEO',
        'capability' => 'manage_options'
    ));

}

